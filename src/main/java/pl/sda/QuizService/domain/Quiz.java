package pl.sda.QuizService.domain;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
@Entity
public class Quiz {
    @Id
    @GeneratedValue
    private Integer id;
    @Column
    private String title;
    @Column
    private String description;
    @Column
    private String date;
    @OneToMany
    @JoinColumn(name = "id")
    private List<Question> questions;

    public Quiz(String date, String title, String description, List<Question> questions) {
        this.date = date;
        this.title = title;
        this.description = description;
        this.questions = questions;
    }

    public Quiz() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
