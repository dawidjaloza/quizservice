package pl.sda.QuizService.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.QuizService.dto.UserDto;
import pl.sda.QuizService.services.UserService;
import pl.sda.QuizService.util.EmailExistsException;
import pl.sda.QuizService.util.Result;
import pl.sda.QuizService.util.ResultType;

@Controller
public class RegistrationController {
    private final UserService service;

    public RegistrationController(UserService service) {
        this.service = service;
    }

    @GetMapping("/register")
    public String showRegistrationForm(Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute("user", userDto);
        return "registration";
    }

    @PostMapping("/register")
    public String registerUser(@ModelAttribute("user") UserDto userDto, Model model) {
        Result result = new Result();
        userDto.setRole("USER");
        userDto.setEnabled(1);
        if (checkUserAttributes(userDto)) {
            try {
                service.registerNewUserAccount(userDto);
            } catch (EmailExistsException e) {
                e.printStackTrace();
                result.setType(ResultType.FAILED);
                result.setDescription(e.getMessage());
            }
            result.setType(ResultType.SUCCESSFUL);
            result.setDescription("Registration successful");
        }
        model.addAttribute("result", result);
        return "registration";
    }

    private boolean checkUserAttributes(UserDto user) {
        if (user == null) return false;
        boolean firstNameCheck = StringUtils.isNotEmpty(user.getFirstName());
        boolean loginCheck = StringUtils.isNotEmpty(user.getUsername());
        boolean passCheck = StringUtils.isNotEmpty(user.getPassword());
        return firstNameCheck && loginCheck && passCheck;
    }
}
