package pl.sda.QuizService.dto;

public class UserAnswer {
    private Integer idAnswer;
    private boolean checkedAnswer;
    private String label;

    public Integer getIdAnswer() {
        return idAnswer;
    }

    public void setIdAnswer(Integer idAnswer) {
        this.idAnswer = idAnswer;
    }

    public boolean isCheckedAnswer() {
        return checkedAnswer;
    }

    public void setCheckedAnswer(boolean checkedAnswer) {
        this.checkedAnswer = checkedAnswer;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
