package pl.sda.QuizService.services;

import pl.sda.QuizService.domain.Question;
import pl.sda.QuizService.domain.Quiz;

import java.util.List;
import java.util.Optional;

public interface QuizService {

    Integer pointsCounter(boolean isCorrect, Integer points);
    Optional<Quiz> getQuiz(Integer id);


}
