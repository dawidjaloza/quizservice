package pl.sda.QuizService.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.QuizService.domain.Statistics;

import java.util.List;

public interface StatisticsRepository extends JpaRepository<Statistics, Integer> {
    List<Statistics> findFirst3ByResultAndId(Double result, Integer id);
}
