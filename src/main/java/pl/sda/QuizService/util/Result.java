package pl.sda.QuizService.util;

public class Result {
    private ResultType type;
    private String description;

    public ResultType getType() {
        return type;
    }

    public void setType(ResultType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Result(ResultType type, String description) {
        this.type = type;
        this.description = description;
    }

    public Result() {
    }
}
