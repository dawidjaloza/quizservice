package pl.sda.QuizService.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Question {
    @Id
    @GeneratedValue
    private Integer id;
    @Column
    private String content;
    @Column
    private Integer points;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "id")
    private List<Answer> answerList;

    public Question() {
    }

    public Question(String content, Integer points, List<Answer> answerList) {
        this.content = content;
        this.points = points;
        this.answerList = answerList;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public List<Answer> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<Answer> answerList) {
        this.answerList = answerList;
    }

    public Integer getId() {
        return id;
    }
}
