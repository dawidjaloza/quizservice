package pl.sda.QuizService.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.QuizService.domain.Quiz;
import pl.sda.QuizService.services.QuizService;

import java.util.Optional;

@Controller
@RequestMapping("/quiz")
public class QuizController {

    @Autowired
    private QuizService service;

    @GetMapping("/quiz/{id}")
    public String getQuiz(Model model, @PathVariable Integer id){
        Optional<Quiz> quizOpt = service.getQuiz(id);
        if(quizOpt.isPresent()) {
            model.addAttribute("quiz", quizOpt.get());
        }
        return "quiz";
    }

}
