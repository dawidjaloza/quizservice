package pl.sda.QuizService.util;

public class EmailExistsException extends Exception {
    public EmailExistsException(String message) {
        super(message);
    }

    public EmailExistsException() {
    }
}
