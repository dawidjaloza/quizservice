package pl.sda.QuizService.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.QuizService.domain.Answer;
import pl.sda.QuizService.repo.AnswerRepository;
import pl.sda.QuizService.services.AnswerService;

import java.util.Optional;


@Service
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    private AnswerRepository repository;

    @Override
    public Optional<Answer> getAnswerById(Integer id) {
        return repository.findById(id);
    }
}
