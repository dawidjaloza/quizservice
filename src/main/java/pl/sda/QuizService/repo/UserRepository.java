package pl.sda.QuizService.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.QuizService.domain.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Override
    boolean existsById(Integer integer);

    boolean existsByUsername(String username);

    Optional<User> findByUsername(String username);

}
