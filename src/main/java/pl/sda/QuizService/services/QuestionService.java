package pl.sda.QuizService.services;

import pl.sda.QuizService.domain.Question;
import pl.sda.QuizService.domain.Quiz;

import java.util.List;
import java.util.Optional;

public interface QuestionService {

    List<Question> listAllQuestions();
    Optional<Question> addQuestion(Question question);
    Optional<Question> getQuestionById(Integer id);
    void deleteQuestion(Integer id);

}
