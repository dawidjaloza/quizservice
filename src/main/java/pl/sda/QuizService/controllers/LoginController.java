package pl.sda.QuizService.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.QuizService.dto.UserDto;
import pl.sda.QuizService.util.Result;
import pl.sda.QuizService.util.ResultType;

@Controller
public class LoginController {
    @GetMapping("/login")
    public String showLoginPage(Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute("user", userDto);
        return "login";
    }

    @RequestMapping("/login-error")
    public String loginError(Model model) {
        Result result = new Result(ResultType.FAILED, "Invalid user or password");
        model.addAttribute("result", result);
        UserDto userDto = new UserDto();
        model.addAttribute("user", userDto);
        return "login";
    }
}
