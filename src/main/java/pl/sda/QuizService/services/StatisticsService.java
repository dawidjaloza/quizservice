package pl.sda.QuizService.services;

import pl.sda.QuizService.domain.Quiz;
import pl.sda.QuizService.domain.Statistics;

import java.util.List;

public interface StatisticsService {

    List<Statistics> topStatistics(Integer id, Statistics statistics);
}
