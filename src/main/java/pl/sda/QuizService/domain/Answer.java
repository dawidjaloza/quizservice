package pl.sda.QuizService.domain;

import javax.persistence.*;

@Entity
public class Answer {
    @Id
    @GeneratedValue
    private Integer id;
    @Column
    private String content;
    @Column
    private boolean isCorrect;

    public Answer() {
    }

    public Answer(String content, boolean isCorrect) {
        this.content = content;
        this.isCorrect = isCorrect;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }
}
