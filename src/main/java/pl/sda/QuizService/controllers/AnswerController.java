package pl.sda.QuizService.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.QuizService.domain.Answer;
import pl.sda.QuizService.services.AnswerService;

import java.util.Optional;

@Controller
@RequestMapping("/answer")
public class AnswerController {

    @Autowired
    private AnswerService service;

    @GetMapping("/answer/{id}")
    public String showAnswers(@PathVariable Integer id, Model model) {
        Optional<Answer> answerOpt = service.getAnswerById(id);
        if(answerOpt.isPresent()) {
            model.addAttribute("answer", answerOpt.get());
        }
        return "quiz";
    }
}
