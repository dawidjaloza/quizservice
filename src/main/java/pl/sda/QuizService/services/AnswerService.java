package pl.sda.QuizService.services;

import pl.sda.QuizService.domain.Answer;

import java.util.Optional;

public interface AnswerService {
    Optional<Answer> getAnswerById(Integer id);
}
