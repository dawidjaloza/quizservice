package pl.sda.QuizService.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.QuizService.domain.Question;
import pl.sda.QuizService.domain.Quiz;
import pl.sda.QuizService.repo.QuestionRepository;
import pl.sda.QuizService.services.QuestionService;
import pl.sda.QuizService.services.QuizService;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepository repository;

    public QuestionServiceImpl() {
    }


    @Override
    public List<Question> listAllQuestions() {
        List<Question> result = new LinkedList<>();
        repository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public Optional<Question> getQuestionById(Integer id) {
        return repository.findById(id);
    }

    @Override
    public Optional<Question> addQuestion(Question question) {
        if (question != null) {
            repository.save(question);
        } else {
            throw new RuntimeException("Can't be null ! ");
        }
        return getQuestionById(question.getId());
    }

    @Override
    public void deleteQuestion(Integer id) {
        repository.deleteById(id);
    }



}


