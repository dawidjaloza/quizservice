package pl.sda.QuizService.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.QuizService.domain.User;
import pl.sda.QuizService.dto.UserDto;
import pl.sda.QuizService.repo.UserRepository;
import pl.sda.QuizService.services.UserService;
import pl.sda.QuizService.util.EmailExistsException;

import java.util.Optional;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository repository;
    @Autowired
    private PasswordEncoder passEncoder;

    public Optional<User> getUserById(Integer id) {
        return repository.findById(id);
    }

    public Optional<User> getUserByLogin(String username) {
        return repository.findByUsername(username);
    }

    public User registerNewUserAccount(UserDto userDto) throws EmailExistsException {
        String login = userDto.getUsername();
        if (userExist(login)) {
            throw new EmailExistsException("This username already exist");
        }
        User user = new User();
        user.setUsername(login);
        user.setFirstName(userDto.getFirstName());
        user.setAuthority(userDto.getRole());
        user.setEnabled(userDto.getEnabled());

        user.setPassword(passEncoder.encode(userDto.getPassword()));
        return repository.save(user);
    }

    public boolean userExist(String login) {
        return repository.existsByUsername(login);
    }

    public Optional<String> getUserEncodedPassword(String login) {
        Optional<User> userOpt = repository.findByUsername(login);
        if (userOpt.isPresent()) {
            String pass = userOpt.get().getPassword();
            return Optional.ofNullable(pass);
        }
        return Optional.empty();
    }

    public boolean processLogin(String login, String password) {
        Optional<String> encodedPassOpt = getUserEncodedPassword(login);
        if (encodedPassOpt.isPresent()) {
            String encodedPass = encodedPassOpt.get();
            return passEncoder.matches(password, encodedPass);
        }
        return false;
    }

}
