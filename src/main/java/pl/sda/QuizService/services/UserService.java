package pl.sda.QuizService.services;

import pl.sda.QuizService.domain.User;
import pl.sda.QuizService.dto.UserDto;
import pl.sda.QuizService.util.EmailExistsException;

import java.util.Optional;

public interface UserService {
    Optional<User> getUserById(Integer id);

    Optional<User> getUserByLogin(String login);

    User registerNewUserAccount(UserDto userDto) throws EmailExistsException;

    boolean userExist(String login);

    Optional<String> getUserEncodedPassword(String login);

    boolean processLogin(String login, String password);
}
