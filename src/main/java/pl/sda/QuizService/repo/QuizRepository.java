package pl.sda.QuizService.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.sda.QuizService.domain.Answer;
import pl.sda.QuizService.domain.Question;
import pl.sda.QuizService.domain.Quiz;

import java.util.Optional;

@Repository
public interface QuizRepository extends CrudRepository<Quiz, Integer> {
    Optional<Quiz> findQuizById(Integer id);
}
