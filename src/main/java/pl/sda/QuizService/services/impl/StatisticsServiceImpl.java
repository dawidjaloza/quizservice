package pl.sda.QuizService.services.impl;

import org.springframework.stereotype.Service;
import pl.sda.QuizService.domain.Quiz;
import pl.sda.QuizService.domain.Statistics;
import pl.sda.QuizService.repo.StatisticsRepository;
import pl.sda.QuizService.services.StatisticsService;

import java.util.List;
import java.util.Optional;

@Service
public class StatisticsServiceImpl implements StatisticsService {

    private StatisticsRepository repository;

    public StatisticsServiceImpl(StatisticsRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Statistics> topStatistics(Integer id, Statistics statistics) {
        return repository.findFirst3ByResultAndId(statistics.getResult(), id);
    }




}
