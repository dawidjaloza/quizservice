package pl.sda.QuizService.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.QuizService.domain.Question;
import pl.sda.QuizService.services.QuestionService;

import java.util.Optional;

@Controller
@RequestMapping("/question")
public class QuestionController {

    @Autowired
    private QuestionService service;

    @GetMapping("/question/{id}")
    public String showQuestion(@PathVariable Integer id, Model model) {
        Optional<Question> questionOpt = service.getQuestionById(id);
        if(questionOpt.isPresent()) {
            model.addAttribute("question", questionOpt.get());
        }
        return "quiz";
    }


}
