package pl.sda.QuizService.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.sda.QuizService.domain.Answer;

@Repository
public interface AnswerRepository extends CrudRepository<Answer, Integer> {
}
