package pl.sda.QuizService.domain;

import javax.persistence.*;

@Entity
public class Statistics {
    @Id
    @GeneratedValue
    private Integer id;
    @OneToOne
    @MapsId
    private User user;
    @OneToOne
    @MapsId
    private Quiz quiz;
    @Column
    private Double result;

    public Statistics(User user, Quiz quiz, Double result) {
        this.user = user;
        this.quiz = quiz;
        this.result = result;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }

    public Statistics() {
    }
}
