package pl.sda.QuizService.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.QuizService.domain.Question;
import pl.sda.QuizService.domain.Quiz;
import pl.sda.QuizService.repo.QuizRepository;
import pl.sda.QuizService.services.QuizService;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class QuizServiceImpl implements QuizService {

    @Autowired
    private QuizRepository repository;

    public QuizServiceImpl() {
    }

    @Override
    public Integer pointsCounter(boolean isCorrect, Integer points) {
        Integer pointsFromQuiz = 0;

        if (isCorrect) {
            pointsFromQuiz += points;
        }

        return pointsFromQuiz;
    }

    @Override
    public Optional<Quiz> getQuiz(Integer id) {
        return repository.findQuizById(id);
    }

}

